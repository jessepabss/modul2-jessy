@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-7">
      <img class="w-100" src="{{ url('storage/'.$detailpost[0]->image) }}" alt="" height="100%" width="100%">
    </div>
    <div class="col-5">
      <img class="rounded-circle" src="{{ url('/storage/'.$detailpost[0]->users->avatar) }}" alt="" height="50px" width="50px">
      {{ $detailpost[0]->users->name }}
      <br>
      <hr>
      <p>
        <b>{{ $detailpost[0]->users->email }}</b> {{ $detailpost[0]->caption }}
      </p>
      @foreach($detailpost[0]->komentar_post as $komentar)
        <b>{{ $komentar->users->email }}</b> {{ $komentar->comment }}
        <br>
      @endforeach
      <form action="/like/{{ $detailpost[0]->id }}" method="get" style="display:inline">
        @csrf
        <button type="submit" class="btn"><i class="fa fa-heart-o"></i></button>
      </form>
      <i class="fa fa-comment"></i>
      <p>{{ $detailpost[0]->likes }} Likes</p>
      <form action="/komen/{{ $detailpost[0]->id }}" method="get">
        @csrf
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Recipient's username" name="komentar">
          <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit">Post</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
