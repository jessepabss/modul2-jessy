@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center my-5">
    <div class="col-2 text-center">
      <img class="rounded-circle" src="{{ url('storage/'.$hasil_data->avatar) }}" alt="" height="150px" width="150px">
    </div>
    <div class="col-9">
      <h2>{{ $hasil_data->name }}</h2>
      <br>
      <a href="/editprofil">Edit Profile</a>
      <p><b>{{ $hasil_data->posts->count() }}</b> Post</p>
      <div class="">
        <b>{{ $hasil_data->title }}</b><br>
        {{ $hasil_data->description }}<br>
        <a href="https://{{ $hasil_data->url }}">{{ $hasil_data->url }}</a><br>
      </div>
    </div>
    <div class="col-1">
      <a href="/tambahpost">Add New Post</a>
    </div>
  </div>
  <div class="row justify-content-left">
    @foreach($hasil_data->posts as $post)
    <div class="col-md-4">
      <a href="/detailpost/{{ $hasil_data->id }}">
        <img src="{{ url('storage/'.$post->image) }}" alt="foto" height="200px" width="100%">
      </a>
    </div>
    @endforeach
  </div>
</div>
</div>
@endsection
