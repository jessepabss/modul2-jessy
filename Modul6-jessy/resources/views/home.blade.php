@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    @foreach($post as $post)
    <div class="col-md-8">
      <div class="card my-3">
        <div class="card-header">
          <img class="rounded-circle" src="{{ url('/storage/'.$post->users->avatar) }}" alt="foto" height="50px" width="50px">
          {{ $post->users->name }}
        </div>
        <div class="card-body">
          <a href="{{ url('/detailpost/'.$post->id) }}">
            <img src="{{ url('/storage/'.$post->image) }}" alt="foto-konten" width="100%">
          </a>
        </div>
        <div class="card-footer">
          <form action="/like/{{ $post->id }}" method="get" style="display:inline">
            @csrf
            <button type="submit" class="btn" value="{{ $post->id }}"><i class="fa fa-heart-o"></i></button>
          </form>
          <i class="fa fa-comment"></i>
          <p>{{ $post->likes }} Likes</p><br>
          <b>{{ $post->users->email }}</b> {{ $post->caption }}
          <hr>
          @foreach($post->komentar_post as $komentar)
            <b>{{ $komentar->users->email }}</b> {{ $komentar->comment }}<br>
          @endforeach
          <form action="/komen/{{ $post->id }}" method="get">
            @csrf
            <div class="input-group mb-3">
              <input type="text" class="form-control" placeholder="Recipient's username" name="komentar">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit" name="button_komen" value="{{ $post->id }}">Post</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection
