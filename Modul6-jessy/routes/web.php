<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profil', 'HomeController@profil');
Route::get('/editprofil', 'HomeController@editprofil');
Route::post('/editprofil-prs', 'HomeController@editprofilprs');
Route::get('/tambahpost', 'HomeController@tambah');
Route::post('/prosestambahpost', 'HomeController@tambahproses');
Route::get('/like/{id}', 'HomeController@like');
Route::get('/komen/{id}', 'HomeController@komen');
Route::get('/detailpost/{id}', 'HomeController@detailpost');
