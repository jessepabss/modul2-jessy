<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  protected $table = "posts";

  public function users(){
    return $this->belongsTo('App\User', 'user_id', 'id');
  }

  public function komentar_post(){
    return $this->hasMany('App\Komentar', 'post_id', 'id');
  }
}
