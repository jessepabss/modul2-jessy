<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar_posts';

    public function users(){
      return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function posts(){
      return $this->belongsTo('App\Post', 'post_id', 'id');
    }
}
