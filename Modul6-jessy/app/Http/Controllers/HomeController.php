<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Post as post;
use App\Komentar as komentar;
use App\User as user;
use Auth;
use DB;



class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index()
  {
    $datapost = post::with('users')->with('komentar_post')->get();
    return view('home', ['post'=>$datapost]);
  }


  public function profil(){
    $id_user = Auth::user()->id;
    $datapost = User::with('posts')->where('id', $id_user)->get();
    return view('profil', ['hasil_data' => $datapost]);
  }

  public function editprofil(){
    $id_user = Auth::user()->id;
    $data_user = User::where('id', $id_user)->get();
    return view('editprofil', ['data_user' => $data_user]);
  }


  public function tambah(){
    return view('tambahpost');
  }
  public function tambahproses(Request $request){
    $gambar = $request->file('gambar')->store('postingan', 'public');
    DB::table('posts')->insert([
      'user_id' => Auth::user()->id,
      'caption' => $request->caption,
      'image' => $gambar,
    ]);
    return redirect('/home');
  }


  public function detailpost($id){
    $datapost = post::with('users')->with('komentar_post')->where('id', $id)->get();
    return view('detailpost', ['detailpost' => $datapost] );
  }

  public function editprofilprs(Request $request){
    $id_user = Auth::user()->id;
    $data_user = User::where('id', $id_user)->get();
    $simpan = $data_user->avatar;
    if ($request->has('profile')) {
      $simpan = $request->file('profile')->store('avatar', 'public');
    }
    DB::table('users')->where('id', $id_user)->update([
      'title' => $request->title,
      'description' => $request->description,
      'url' => $request->url,
      'avatar' => $simpan,
    ]);
    return redirect('/profil');
  }
  public function like($id){
    $likes = post::where('id', $id)->increment('likes');
    return redirect('/home');
  }

  public function komen($id,Request $request){
    $komentar = $request->komentar;
    $id_user = Auth::user()->id;
    $id_post = $id;
    DB::table('komentar_post')->insert([
      'user_id' => $id_user,
      'post_id' => $id_post,
      'comment' => $komentar,
    ]);
    return redirect('/home');
  }
}
