<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class postseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('posts')->insert([
        'user_id' => 1,
        'caption' => 'Waktu saya pergi ke paris',
        'image' => 'paris.jpg',
      ]);

      DB::table('posts')->insert([
        'user_id' => 2,
        'caption' => 'Telkom University',
        'image' => 'telkom.jpg',
      ]);
    }
}
