<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class userseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'name' => 'Jessy Anugerah',
        'email' => 'jessy@gmail.com',
        'password' => bcrypt('jessy'),
        'avatar' => 'jessy.jpg',
      ]);

      DB::table('users')->insert([
        'name' => 'Pablo',
        'email' => 'pabs@gmail.com',
        'password' => bcrypt('pabs'),
        'avatar' => 'pabs.jpg',
      ]);
    }
}
