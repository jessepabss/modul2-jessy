@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    @foreach($hasil as $data)
    <div class="col-md-8">
      <div class="card my-3">
        <div class="card-header">
          <p>
            <img src="{{ $data->avatar }}" alt="" class="rounded-circle" height="50" width="50">
            {{ $data->name }}
          </p>
        </div>
        <div class="card-body">
          <img src="{{ $data->image }}" alt="" width="100%">
        </div>
        <div class="card-footer">
          <div class="text-left">
            <strong>{{ $data->email }}</strong>
          </div>
          <p>{{ $data->caption }}</p>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection
