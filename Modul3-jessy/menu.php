<!doctype html>
<html lang="en">
<head>

	<title>Menu</title>

	<style>

		h1, p{
			text-align: center;
		}
		.kiri{
			text-align: center;
			position: fixed;
			margin: 36px 97px;
			padding-left: 143px;
		}
		.kanan{
			margin: 36px 97px;
			float: right;
			padding-right: 143px;
		}
		.batas{
			border-bottom: 1px solid grey;
			padding: 15px;
		}

		*{
			font-family: arial;
		}
	</style>

</head>
<body>

	<?php 

	if (isset($_GET['kantong']) && $_GET['kantong'] == "Bawa") {
		$kantongbelanja = "Bawa";
	}else{
		$kantongbelanja = "Tidak Bawa";
	}

	$nama = $_POST['nama'];
	$nomor = $_POST['nomor'];
	$tanggal = $_POST['tanggal'];
	$driver = $_POST['driver'];

	?>

	<div class="kiri" align="left">
		<h1>~Data Driver Ojol~</h1>
		<p><b>Nama</b></p>
		<p><?php echo $nama; ?></p>
		<p><b>Nomor Telepon</b></p>
		<p><?php echo $nomor; ?></p>
		<p><b>Tanggal</b></p>
		<p><?php echo $tanggal; ?></p>
		<p><b>Asal Driver</b></p>
		<p><?php echo $driver; ?></p>
		<p><b>Bawa Kantong</b></p>
		<p><?php echo $kantongbelanja; ?></p>
		<center>
			<button onclick="window.location.href='form.html'" type="button" style="background-color: purple; padding: 9px; color: white; border: none;"><< Kembali</button>
		</center>
	</div>
	<div class="kanan">
		<h1>~Menu~</h1>
		<p>Pilih Menu</p>
		<form action="nota.php" method="post" onsubmit="return confirm('Apakah Anda Yakin?');">
			<div class="batas">
				<div style="display: inline-block;min-width: 200px;">
					<input type="checkbox" name="harga[]" value="28000">
					<label>
						<b>Es Coklat Susu
						</b>
					</label>
				</div>
				Rp. 28.000,-
			</div>
			<div class="batas">
				<div style="display: inline-block;min-width: 200px;">
					<input type="checkbox" name="harga[]" value="18000">
					<label>
						<b>Es Susu Matcha
						</b>
					</label>
				</div>
				Rp. 18.000,-
			</div>
			<div class="batas">
				<div style="display: inline-block;min-width: 200px;">
					<input type="checkbox" name="harga[]" value="15000">
					<label>
						<b>Es Susu Mojicha
						</b>
					</label>
				</div>
				Rp. 15.000,-
			</div>
			<div class="batas">
				<div style="display: inline-block;min-width: 200px;">
					<input type="checkbox" name="harga[]" value="30000">
					<label>
						<b>Es Matcha Latte
						</b>
					</label>
				</div>
				Rp. 30.000,-
			</div>
			<div class="batas">
				<div style="display: inline-block;min-width: 200px;">
					<input type="checkbox" name="harga[]" value="21000">
					<label>
						<b>Es Taro Susu
						</b>
					</label>
				</div>
				Rp. 21.000,-
			</div>
			<div class="batas">
				<div style="display: inline-block;min-width: 200px;">
					<label>Nomor Order</label>
				</div>
				<input type="number" name="nomor" required>
			</div>
			<div class="batas">
				<div style="display: inline-block;min-width: 200px;">
					<label>Nama Pemesan</label>
				</div>
				<input type="text" name="nama" required>
			</div>
			<div class="batas">
				<div style="display: inline-block;min-width: 200px;">
					<label >Email</label>
				</div>
				<input type="email" name="email" required>
			</div>
			<div class="batas">
				<div style="display: inline-block;min-width: 200px;">
					<label>Alamat Order</label>
				</div>
				<input type="text" name="alamat" required>
			</div>
			<div class="batas">
				<div style="display: inline-block;min-width: 200px;">
					<label >Member</label>
				</div>
				<input type="radio" name="member" value="Ya" required>
				<label>Ya</label>
				<input type="radio" name="member" value="Tidak" required>
				<label>Tidak</label>
			</div>
			<div class="batas">
				<div style="display: inline-block;min-width: 200px;">
					<label >Metode Pembayaran</label>
				</div>
				<select name="metode" required>
					<option value="">Pilih Metode Pembayaran</option>
					<option value="Cash">Cash</option>
					<option value="E-Money (OVO/Gopay)">E-Money (OVO/Gopay)</option>
					<option value="Credit Card">Credit Card</option>
					<option value="Lainnya">Lainnya</option>
				</select>
			</div>
			<center>
				<button type="submit" style="min-width: 100%; margin:20px 0px; background-color: purple; padding: 9px; color: white; border: none;" name="cetak">CETAK STRUK</button>
			</center>
		</form>
	</div>
</body>
</html>