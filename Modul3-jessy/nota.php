<!doctype html>
<html lang="en">
<head>
	<title>Form</title>

	<style>
		h1, p{
			text-align: center;
		}

		.tengah{
			margin: 50px 393px;
			padding: 20px;
		}

		.batas{
			border-bottom: 1px solid grey;
			padding: 15px;
		}

		*{
			font-family: arial;
		}
	</style>

</head>
<body>

	<?php 

	$jumlah = 0;
	if (isset($_POST['harga'])) {
		$harga = $_POST['harga'];
		foreach ($harga as $harga) {
			$jumlah += $harga;
		}
	}else{
		$harga = 0;
	}

	$member = $_POST['member'];
	if ($member == "Ya") {
		$jumlah += $jumlah * (10/100);
	}
	$jumlah = number_format($jumlah, 2,",",".");

	$nomor = $_POST['nomor'];
	$nama = $_POST['nama'];
	$email = $_POST['email'];
	$alamat = $_POST['alamat'];
	$metode = $_POST['metode'];

	?>

	<h1>Transaksi Pemesanan</h1>
	<p>Terima kasih telah berbelanja Kopi Susu Duarrr!</p>
	<h1 class="tengah">Rp. <?php echo $jumlah ?>,-</h1>
	<div class="tengah">
		<div class="batas">
			<div style="display: inline-block;min-width: 200px;">
				<label><b>ID</b></label>
			</div>
			<?php echo $nomor; ?>
		</div>
		<div class="batas">
			<div style="display: inline-block;min-width: 200px;">
				<label><b>Nama</b></label>
			</div>
			<?php echo $email; ?>
		</div>
		<div class="batas">
			<div style="display: inline-block;min-width: 200px;">
				<label><b>Email</b></label>
			</div>
			<?php echo $email; ?>
		</div>
		<div class="batas">
			<div style="display: inline-block;min-width: 200px;">
				<label><b>Alamat</b></label>
			</div>
			<?php echo $alamat; ?>
		</div>
		<div class="batas">
			<div style="display: inline-block;min-width: 200px;">
				<label><b>Member</b></label>
			</div>
			<?php echo $member; ?>
		</div>
		<div class="batas">
			<div style="display: inline-block;min-width: 200px;">
				<label><b>Pembayaran</b></label>
			</div>
			<?php echo $metode; ?>
		</div>
	</div>

</body>
</html>